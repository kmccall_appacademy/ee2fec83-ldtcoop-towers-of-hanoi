# Towers of Hanoi
#
# Write a Towers of Hanoi game:
# http://en.wikipedia.org/wiki/Towers_of_hanoi
#
# In a class `TowersOfHanoi`, keep a `towers` instance variable that is an array
# of three arrays. Each subarray should represent a tower. Each tower should
# store integers representing the size of its discs. Expose this instance
# variable with an `attr_reader`.
#
# You'll want a `#play` method. In a loop, prompt the user using puts. Ask what
# pile to select a disc from. The pile should be the index of a tower in your
# `@towers` array. Use gets
# (http://andreacfm.com/2011/06/11/learning-ruby-gets-and-chomp.html) to get an
# answer. Similarly, find out which pile the user wants to move the disc to.
# Next, you'll want to do different things depending on whether or not the move
# is valid. Finally, if they have succeeded in moving all of the discs to
# another pile, they win! The loop should end.
#
# You'll want a `TowersOfHanoi#render` method. Don't spend too much time on
# this, just get it playable.
#
# Think about what other helper methods you might want. Here's a list of all the
# instance methods I had in my TowersOfHanoi class:
# * initialize
# * play
# * render
# * won?
# * valid_move?(from_tower, to_tower)
# * move(from_tower, to_tower)
#
# Make sure that the game works in the console. There are also some specs to
# keep you on the right track:
#
# ```bash
# bundle exec rspec spec/towers_of_hanoi_spec.rb
# ```
#
# Make sure to run bundle install first! The specs assume you've implemented the
# methods named above.

class TowersOfHanoi
  def initialize
    @towers = [[3, 2, 1], [], []]
  end

  attr_reader :towers

  def self.play
    loop do
      print "Would you like to play Towers of Hanoi? (y/n)"
      response = gets.chomp
      if response == "y"
        break
      elsif response == "n"
        abort("Come again soon!")
      else
        puts "I'm sorry, what was that?"
      end
    end
    TowersOfHanoi.new
  end

  def move(from_tower, to_tower)
    if valid_move?(from_tower, to_tower)
      @towers[to_tower].push(@towers[from_tower].pop)
    end
  end

  def valid_move?(from_tower, to_tower)
    # no luck if this is true
    if @towers[from_tower].empty?
      puts "You are trying to take from an empty tower!"
      puts "You can't take something from nothing!"
      false
    end
    # auto-succeed if above is false and this is true
    return true if @towers[to_tower].empty?
    proper_stacking = @towers[from_tower].last < @towers[to_tower].last
    if proper_stacking
      true
    else
      puts "You can't put a large disk on top of a smaller disk!"
      false
    end
  end

  def render
    # temporary render for tests
    p @towers
  end

  def won?
    won_stack = [3, 2, 1]
    if @towers[1] == won_stack || @towers[2] == won_stack
      true
    else
      false
    end
  end

  def tower_get(message)
    response = ""
    loop do
      puts message
      response = gets.chomp.to_i
      break if self.valid_get?(response)
    end
    response
  end

  def valid_get?(response)
    if response == 0 || response == 1 || response == 2
      true
    else
      puts "No such tower! Please enter a tower from 0 to 2!"
      false
    end
  end
end

loop do
  game = TowersOfHanoi::play
  until game.won?
    game.render
    from = game.tower_get("From which tower would you like to take? (0-2)")
    to = game.tower_get("Where would you like to put that disk? (0-2)")
    game.move(from, to)
  end
  puts "Congradulations! You won!"
  puts "Type 'again' to play again, or just press RETURN to exit."
  answer = gets.chomp
  abort("Thanks for playing!") unless answer == "again"
end
